console.log("------FUNCTIONS------")
// Task 1

const mapOfNumberName = { 1: "one", 2: "two", 3: "three", 4: "four", 5: "five", 
    6: "six", 7: "seven", 8: "eight", 9: "nine", 0: "zero"}

function getLastNameOfNumber(number) {
    const lastDigit = number % 10
    return mapOfNumberName[lastDigit]
}
console.log("Task 1: Last number name: ")
console.log(getLastNameOfNumber(1996))

// Task 2

function reverseNumber(number) {
    return Number(number
        .toString()
        .split("")
        .reverse()
        .join(""))
}
console.log("Task 2: Reverse number")
console.log(reverseNumber(1234))

// Task 3
const stringForTask3 = "Hello. My name is Max. This is my code"
function findMatchWord(str, word, caseOnOff) {
    let regCase;
    if(caseOnOff) 
        return str.match(new RegExp(word, 'gi')).length; 
    else 
        return str.match(new RegExp(word, 'g')).length;
    
}
console.log("Task 3: Found " + 
findMatchWord(stringForTask3, 'my', caseOnOff=true) + 
" matches 'my' in string " + stringForTask3)

// Task 4

//function countOfDivElem() {
//    return document.getElementsByTagName('div').length;
//}

//console.log("Task 4: Count of div element: " + countOfDivElem())

// Task 5

function countOfNumberInArray(array, number) {
    return array.reduce((accum, element) => {
      return element === number ? accum + 1 : accum;
    }, 0);
}

console.log("Task 5: Count of number: " + countOfNumberInArray([1,2,3,4,5,2,3,2], 2))


// Task 6

function equalNumberWithNeighbours(array, element) {
    if (element <= 0 || element >= array.length - 1) {
        return 'No such elements';
    }
    return array[element] > array[element - 1] && array[element] > array[element + 1];
}

console.log("Task 6: Element more than neighbors: " 
    + equalNumberWithNeighbours([1,2,3,4,5,2,3,2], 4));

console.log("Task 6: Element more than neighbors: " +
    equalNumberWithNeighbours([1,2,3], 0));
  