// Task 1

function reverseString(str) {
    return str
        .split("")
        .reverse()
        .join("")
}
console.log("Task 1: Reverse string is " + reverseString("sample"));

// Task 2
const strWithValidParent = "((a + b) / 5-d)"
const strWithInValidParent = ")(a + b) / 5-d)"

function checkParenthesesInText(str) {
    var openedParentheses = 0;
    var closedParentheses = 0;

    for(var i of str) {
        if( i == '(') {
            openedParentheses++;
        } 
        else if(i == ')') {
            closedParentheses++;
        }
    }
    return closedParentheses == openedParentheses ? "Valid statement!" : "Invalid statement!";
}
console.log("Task 2: Check Parentheses In Text " + strWithValidParent + ": " 
    + checkParenthesesInText(strWithValidParent));

console.log("Check Parentheses In Text " + strWithInValidParent + ": " 
    + checkParenthesesInText(strWithInValidParent));

// Task 3

function matchInText(str) {
    return str.match(/in/gi).length;
}
console.log(matchInText("We are liv<b>in</b>g **in** an yellow submar<b>in</b>e. " + 
"We don't have anyth<b>in</b>g else. **In**side the submar<b>in</b>e is very tight. " + 
"So we are dr<b>in</b>k<b>in</b>g all the day. We will move out of it **in** 5 days."))