console.log("------ARRAYS------")
// Task 1
let array = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20];
let new_array = array.map(index =>index = index * 5);
console.log("Task 1: New array after transformation is: ")
console.log(new_array);


// Task 2
function compareChar(array1, array2) {
    const string1 = array1.join("");
    const string2 = array2.join("");
    if (string1.localeCompare(string2) == 1) {
      return false;
    }
    else 
      return true;
}

console.log("Task 2: Arrays are equal: " + compareChar(['a','b','b'], ['a','b','b']))

// Task3
const arrayOfNumberTask3 = [2, 1, 1, 2, 3, 3, 2, 2, 2, 1];

function maxRangeOfNumber(array) {
    var incSeqArray = [array[0]]
    var tempSeqArray = [array[0]]
    for (let i = 1; i < array.length; i++) {
      if(array[i] == array[i-1]) {
        tempSeqArray.push(array[i]);
      }
      else {
        if(tempSeqArray.length > incSeqArray.length) {
          incSeqArray = tempSeqArray;
        }
        tempSeqArray = [array[i]]
      }
    }
    return incSeqArray;
}

console.log("Task 3: Max range of equal number ")
console.log(maxRangeOfNumber(arrayOfNumberTask3))


// Task 4

const arrayOfNumber = [3, 2, 3, 4, 2, 2, 4];

function maxIncSequence(array) {
    var incSeqArray = [array[0]]
    var tempSeqArray = [array[0]]
    for(var i = 1; i < array.length; i++) {
      if(array[i] > array[i-1]) {
        tempSeqArray.push(array[i]);
      }
      else {
        if(tempSeqArray.length > incSeqArray.length) {
          incSeqArray = tempSeqArray;
        }
        tempSeqArray = [array[i]]
      }
    }
    return incSeqArray;
}
console.log("Task 4: Max increasing sequence: ")
console.log(maxIncSequence(arrayOfNumber))

// Task 5

function swap(items, firstIndex, secondIndex){
  var temp = items[firstIndex];
  items[firstIndex] = items[secondIndex];
  items[secondIndex] = temp;
}

function selectionSort(array) {
  let minItem;
  for (let i = 0; i < array.length; i++) {
      minItem = i;
      for (let j = i + 1; j < array.length; j++) {
          if (array[j] < array[minItem]){
            minItem = j;
          }
      }
      if (i != minItem){
          swap(array, i, minItem);
      }
  }
  return array;
}
console.log("Task 5: Sorted array: ")
console.log(selectionSort([4, 1, 1, 4, 2, 3, 4]))




// Task 6

function maxCountNumber(array) {
  let max_count = 1;
  let num = array[0];
  for (let i = 0; i < array.length - 1; i++) {
    let count = 1;
    for (let j = 1; j < array.length; j++) {
      if(array[i] == array[j]) {
        count++;
      }
    }
    if(count >= max_count) {
      max_count = count;
      num = array[i];
    }
  }
  return "Number " + num + " (" + max_count + " times)";
}

console.log("Task 6: " + maxCountNumber([4, 1, 1, 4, 2, 3, 4, 4, 1, 2, 4, 9, 3]))